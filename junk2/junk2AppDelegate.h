//
//  junk2AppDelegate.h
//  junk2
//
//  Created by Karla Hopp on 12-11-01.
//  Copyright (c) 2012 Karla Hopp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface junk2AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
