//
//  main.m
//  junk2
//
//  Created by Karla Hopp on 12-11-01.
//  Copyright (c) 2012 Karla Hopp. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "junk2AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([junk2AppDelegate class]));
    }
}
